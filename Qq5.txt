package com.Mmap;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.Panel;
import java.awt.TextField;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Qq5 extends JPanel {
	JPanel qq5p2=new JPanel();
	private JLabel qq5l1=null;
	JLabel a1=new JLabel("查 询:");
	Choice c1=new Choice();
	TextField t1=new TextField("        ");
	JButton j1=new JButton("  搜 索   ");
	JButton j2=new JButton("  添 加   ");
	JButton j3=new JButton("  编 辑   ");
	JLabel Jlnew315 = new JLabel();
	JTable jj = new JTable();
	JLabel l1 = new JLabel("首页");
	JLabel l2 = new JLabel("上一页");
	JLabel l3 = new JLabel("第一页");
	JLabel l4= new JLabel("共一页");
	JLabel l5 = new JLabel("下一页");
	JLabel l6 = new JLabel("尾页");
	JCheckBox JCB = new JCheckBox("跳转");
	JLabel l8 = new JLabel("当前共有3条读者信息");
	
	public Qq5() {
		DefaultTableModel ww = new DefaultTableModel(
				new Object[][] { {"序号","读者编号", "姓名", "读者类别", "邮箱", "联系电话","证件号"}, 
					{ "001","RE9674989258","丹东","儿童","5445555@qq.com","13544456777","362430198904014211"},
					{"002","RE9674996030","问飞","老人","234234234@QQ.COM","13755446688","140525197207181222"},
					{"003","RE9674999183","阿斯顿发","军人","324534534@qq.com","13866554466","310108198112305497"}},
				new String[] {"序号","读者编号", "姓名", "读者类别", "邮箱", "联系电话","证件号"});
		String nub=Jlnew315.getText();
		String time=Jlnew315.getText();
		String ad = Jlnew315.getText();
		ww.addRow(new String[] {nub, time,ad});
		qq5l1=new JLabel("   读者信息列表                                                                                                              ");
		qq5l1.setFont((new Font("微软雅黑", Font.PLAIN, 20)));
		qq5l1.setOpaque(true);
		qq5l1.setBorder(BorderFactory.createLineBorder(Color.darkGray));//设置边框颜色
		qq5l1.setBackground(Color.gray);
		qq5l1.setBounds(20, 20,670,100);
		jj.setModel(ww);
		j1.setBounds(0, 0, 820, 50);
		jj.setBorder(BorderFactory.createLineBorder(Color.gray));
		
		Box box1,box2,box3,box4,box;
		box1=Box.createHorizontalBox();
		box1.add(Box.createHorizontalStrut(5));
		box1.add(qq5l1);
		box1.add(Box.createHorizontalStrut(5));
		
		box2=Box.createHorizontalBox();
		box2.add(Box.createHorizontalStrut(5));
		box2.add(a1);
		box2.add(Box.createHorizontalStrut(5));
		box2.add(c1);
		box2.add(Box.createHorizontalStrut(10));
		box2.add(t1);
		box2.add(Box.createHorizontalStrut(10));
		box2.add(j1);
		box2.add(Box.createHorizontalStrut(10));
		box2.add(j2);
		box2.add(Box.createHorizontalStrut(10));
		box2.add(j3);
		box2.add(Box.createHorizontalStrut(10));
		box3=Box.createHorizontalBox();
		box2.add(Box.createHorizontalStrut(10));
		box3.add(jj);
		box2.add(Box.createHorizontalStrut(10));
		box4=Box.createHorizontalBox();
		box4.add(Box.createHorizontalStrut(10));
		box4.add(l1);
		box4.add(Box.createHorizontalStrut(10));
		box4.add(l2);
		box4.add(Box.createHorizontalStrut(10));
		box4.add(l3);
		box4.add(Box.createHorizontalStrut(10));
		box4.add(l4);
		box4.add(Box.createHorizontalStrut(10));
		box4.add(l5);
		box4.add(Box.createHorizontalStrut(10));
		box4.add(l6);
		box4.add(Box.createHorizontalStrut(10));
		box4.add(JCB);
		box4.add(Box.createHorizontalStrut(370));
		box4.add(l8);
		box4.add(Box.createHorizontalStrut(40));
	
		box=Box.createVerticalBox();
		box.add(box1);
		box.add(Box.createVerticalStrut(40));
		box.add(box2);
		box.add(Box.createVerticalStrut(40));
		box.add(box3);
		box.add(Box.createVerticalStrut(40));
		box.add(box4);
		box.add(Box.createVerticalStrut(60));
		qq5p2.add(box);
		
		c1.add("全部读者信息");
		c1.add("按读者编号查询");
		c1.add("按读者姓名查询");
		c1.add("按读者类别查询");
		qq5p2.setBounds(20, 20, 820, 420);
		qq5p2.setBorder(BorderFactory.createTitledBorder("分组框")); //设置面板边框，实现分组框的效果，此句代码为关键代码
		qq5p2.setBorder(BorderFactory.createLineBorder(Color.darkGray));//设置面板边框颜色
		this.add(qq5p2);
	}
}

